const request = require('request')
const express = require('express')
const geocode = require('./utils/geocode.js')
const forecast = require('./utils/forecast.js')
const app =  express()
let port = process.env.PORT || 3000
app.get('/weather',(req,res) => {
    if(!req.query.address){
        return res.send({
            error : 'You must provide an address'
        })
    }
    geocode(req.query.address,(error, {latitude, longitude, location}) => {
        if (error){
            return res.send({error})
        }
                   
        forecast(req.query.address, function(error, forecastData)  {
            
            if (error){
                console.log("enter")
                return res.send({error})

            }
            
            res.send({
                forecast : forecastData,
                location,
                address : req.query.address
            })
        })
    

    })
    // console.log(req.query.address)
    // res.send({
    //     forecast : 'snowy',
    //     location : 'boston',
    //     address : req.query.address

    // })
    
})
app.get('/products',(req,res) => {
    if(!req.query.search)
    {
        return res.send({
            error: 'You must provide a search term'
        })
    }
    console.log(req.query.search)
    res.send({
        products : req.query.search
    })
})
app.listen(port, function(err){
    if(err) throw err
    console.log('server running on port'+ port)
})
