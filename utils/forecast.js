const request = require('request')
const forecast = (address, callback) => {
   // console.log(address)
    const url = 'https://api.weatherapi.com/v1/current.json?key=%209c29079c769d4660a72143354200707&q='+ address
      request({ url : url, json : true}, (error , response) => {
         // console.log(response)
          if(error){
              callback('Unable to connect to weather service!',undefined)
          }
          else if (response.body.error)
          {
              callback('Unable to find location',undefined)
          }else{
        callback(undefined, 'It is currently '+response.body.current.temp_f+' degrees out. There is a '+response.body.current.precip_in+'% chance of rain')
          }
        //console.log(error)
    })
}
module.exports = forecast